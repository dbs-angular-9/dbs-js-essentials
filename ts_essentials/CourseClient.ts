import { CourseService } from "./CourseService";

const client = new CourseService();

const java = {
    id: 1, 
    name: "java", 
    desc: "java course",
    price:15000,
    rating:4.8
}
const javascript = {
    id: 2, 
    name: "javascript", 
    desc: "javascript course",
    price:15000,
    rating:4.8
}
client.addCourse (java);
client.addCourse (javascript);

client.listAllCourses().forEach(c => console.log(c));

console.log("==============================");

console.log(client.fetchCourseById(java));
