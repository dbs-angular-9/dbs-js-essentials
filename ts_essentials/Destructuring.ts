const javascript = {
    id: 2, 
    name: "javascript", 
    desc: "javascript course",
    price:15000,
    rating:4.8
}

const { id: courseId, name:coursename, desc:courseDesc } = javascript;

console.log(`The id is ${courseId} with name ${coursename} : ${courseDesc}`)


function printCourseDetails({name:n, desc: d}){
    console.log(`Name : ${n} with Desc: ${d}`)
}

printCourseDetails(javascript);

//Array destructuring

const players = ["Dhoni", "Kholi",'Dhawan','Yuvraj', "Rohit"];

const [ captain, vCaptain, ...restOfThePlayers] = players;

console.log(`Captain: ${captain} and Vice captain ${vCaptain}`);

console.log("Rest of the players")

restOfThePlayers.forEach(p => console.log(p));

//swapping players;

let batsman = "Ganguly";
let runner = "Tendulkar";

[runner, batsman] = [batsman, runner];

console.log (`After taking a single, Now the Batsman 
              is ${batsman} and Runner is ${runner}`);
