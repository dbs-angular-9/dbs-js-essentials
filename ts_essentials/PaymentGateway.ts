import {Pay} from './User';

export class GooglePay implements Pay {
    pay(from:any, to:any, amount:number){
        return `${amount} paid from ${from} to ${to} using Google Pay`;
    }
}

export class PhonePay {
    pay(from:string, to:string, amount:number){
        return `${amount} paid from ${from} to ${to} using Phone Pay`;
    }
}