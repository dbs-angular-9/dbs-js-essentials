"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PhonePay = exports.GooglePay = void 0;
var GooglePay = /** @class */ (function () {
    function GooglePay() {
    }
    GooglePay.prototype.pay = function (from, to, amount) {
        return amount + " paid from " + from + " to " + to + " using Google Pay";
    };
    return GooglePay;
}());
exports.GooglePay = GooglePay;
var PhonePay = /** @class */ (function () {
    function PhonePay() {
    }
    PhonePay.prototype.pay = function (from, to, amount) {
        return amount + " paid from " + from + " to " + to + " using Phone Pay";
    };
    return PhonePay;
}());
exports.PhonePay = PhonePay;
