import {User, greeter as g, PI, Status, Pay} from './User';
import { GooglePay, PhonePay } from './PaymentGateway';

const ravi = new User("Ravi", undefined, 35);
ravi.firstName = "RAVI";

console.log(`Hello !! ${ravi.firstName}`);
//ravi.firstName = "Ravi";

g("Hi " , ravi);
console.log(Status.MARRIED)

console.log(`PI value: ${PI}`);

let paymentGateway:Pay = new GooglePay();
let paymentGatewayWithPhonePay:Pay = new PhonePay();

const result = paymentGateway.pay(23, 'Vinay', 4000);

console.log(result);
