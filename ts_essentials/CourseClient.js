"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CourseService_1 = require("./CourseService");
var client = new CourseService_1.CourseService();
var java = {
    id: 1,
    name: "java",
    desc: "java course",
    price: 15000,
    rating: 4.8
};
var javascript = {
    id: 2,
    name: "javascript",
    desc: "javascript course",
    price: 15000,
    rating: 4.8
};
client.addCourse(java);
client.addCourse(javascript);
client.listAllCourses().forEach(function (c) { return console.log(c); });
console.log("==============================");
console.log(client.fetchCourseById(java));
