"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CourseService = void 0;
var CourseService = /** @class */ (function () {
    function CourseService() {
        this.courses = [];
    }
    CourseService.prototype.addCourse = function (course) {
        this.courses.push(course);
    };
    CourseService.prototype.listAllCourses = function () {
        return this.courses;
    };
    CourseService.prototype.fetchCourseById = function (_a) {
        var id = _a.id;
        console.log("The id is " + id);
        return this.courses[0];
    };
    return CourseService;
}());
exports.CourseService = CourseService;
