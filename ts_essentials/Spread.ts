const fruits = ['mango', 'grapes','banana','jackfruit'];
const veg = ['potato','brinjal','beans','raddish','carrot'];

const cart =[...fruits, 'litchi', 'guava', ...veg];

cart.forEach(c => console.log(c));