import { Course } from "./Course";

export class CourseService {

    courses:Course[] = [];

    addCourse(course:Course):void{
        this.courses.push(course);
    }

    listAllCourses(): Course[]{
        return this.courses;
    }

    fetchCourseById({id}):Course{
        console.log(`The id is ${id}`)
        return this.courses[0];       
    }
}