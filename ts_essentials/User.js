"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PI = exports.Status = exports.greeter = exports.User = void 0;
var User = /** @class */ (function () {
    function User(firstname, lastname, age) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
    }
    Object.defineProperty(User.prototype, "firstName", {
        get: function () {
            return this.firstname;
        },
        set: function (fName) {
            this.firstname = fName;
        },
        enumerable: false,
        configurable: true
    });
    return User;
}());
exports.User = User;
function greeter(message, user) {
    return message + " !! " + user.firstName;
}
exports.greeter = greeter;
var Status;
(function (Status) {
    Status[Status["MARRIED"] = 0] = "MARRIED";
    Status[Status["UNMARRIED"] = 1] = "UNMARRIED";
})(Status = exports.Status || (exports.Status = {}));
exports.PI = 3.142;
