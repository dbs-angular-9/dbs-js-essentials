var _a;
var javascript = {
    id: 2,
    name: "javascript",
    desc: "javascript course",
    price: 15000,
    rating: 4.8
};
var courseId = javascript.id, coursename = javascript.name, courseDesc = javascript.desc;
console.log("The id is " + courseId + " with name " + coursename + " : " + courseDesc);
function printCourseDetails(_a) {
    var n = _a.name, d = _a.desc;
    console.log("Name : " + n + " with Desc: " + d);
}
printCourseDetails(javascript);
//Array destructuring
var players = ["Dhoni", "Kholi", 'Dhawan', 'Yuvraj', "Rohit"];
var captain = players[0], vCaptain = players[1], restOfThePlayers = players.slice(2);
console.log("Captain: " + captain + " and Vice captain " + vCaptain);
console.log("Rest of the players");
restOfThePlayers.forEach(function (p) { return console.log(p); });
//swapping players;
var batsman = "Ganguly";
var runner = "Tendulkar";
_a = [batsman, runner], runner = _a[0], batsman = _a[1];
console.log("After taking a single, Now the Batsman \n              is " + batsman + " and Runner is " + runner);
