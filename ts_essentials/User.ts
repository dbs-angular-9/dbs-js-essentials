export class User {
    constructor(private firstname:string, 
                private lastname?:string, 
                private age?:number){}

    get firstName():string{
        return this.firstname;
    }
    set firstName(fName:string){
        this.firstname = fName;
    }
}

export function greeter(message: string, user:User):string{
    return `${message } !! ${user.firstName}`;
}

export enum Status {
    MARRIED, UNMARRIED
}

export const PI:number = 3.142;

export interface Pay {
    pay(from:any, to:string, money:number):string 
}