var sum = function(a, b){return a + b};
var result = sum(4,5);

var sumArrowFun = (a,b) => a + b;

var product = function(a, b){return a * b};

var productArrowFun = (a,b) => a * b;

var thisRef = function(){console.log(this)};
thisRef();