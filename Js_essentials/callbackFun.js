var ages = [23, 12, 34, 56, 78, 11, 2, 8, 14, 55];

var ageLT18 = function(age){
    return age < 18;
}
var ageGT60 = function(age){
    return age > 60;
}
//console.log(ageLT18(12));

var agesLt18 = ages.filter(ageLT18);

var seniorCitizens = ages.filter(age => (age > 18 && age  < 68));

var teenagers = ages.filter(age => age > 12 && age < 19 );
var adults = ages.filter(age =>  age > 18 );


//declarative prgramming style
ages.filter(age => age < 18 ) // -> (in java)
    .sort((a, b)=> a - b)
    .reverse()
    .map(age => " Children "+ age)
    .forEach(val => console.log(val));

//common array methods 
/*
  push - add element at the end of the array
  pop - remove element at the end of the array
  unshift - add element at the beginning of the array
  shift - remove element at the beginning of the array
  splice - to add/remove at any arbitary position
  slice - to fetch the elements given a range

*/   


