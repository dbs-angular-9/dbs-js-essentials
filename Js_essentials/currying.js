//currying - partial functions
var calculateTotalBill = function(taxInPercetage){
   return function(amount){
       return function(tip){
           return amount + (taxInPercetage/100 * amount) + tip;
       }
   }
}

var taxInPercetage12 = calculateTotalBill(12);
var taxInPercetage15 = calculateTotalBill(15);
var raviTotalBill = taxInPercetage12(100)
var sureshTotalBill = taxInPercetage12(100)

console.log("The total bill is "+ raviTotalBill(20));
console.log("The total bill is "+ sureshTotalBill(50));
console.log("The total bill is "+ calculateTotalBill(12)(2000)(50));

// configuration
/*
    http://dev.qbo.intuit.com/context
    http://qa.qbo.intuit.com/context
    http://prod.qbo.intuit.com/context

  datasource config
  
  sealed - username, password, connect string

  configurable - idletime, maxconnection, active connection
*/