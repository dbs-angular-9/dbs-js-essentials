//function is an object
var user = {
    first_name:'Ramesh',
    last_name:"Kumar",
    age: 34,
    address:{
        city:'Bengaluru',
        state:'Karnataka'
    },
    hobbies:['singing', "dancing", "playing chess"],

    fullName:function(){
        return this.first_name + " "+this.last_name;
    },
    setFirstName: function(fName){
        this.first_name = fName;
    }
};

//augment the object with methods and values 

user.address.pincode = '577142';

//user.getName = function(){return this.name};
console.log("The full name is "+ user.fullName());