//functions are first class citizens
//java - OO
/*
 1. Assigned to a variable
 2. Object passed as an argument to a method 
 3. Object returned from a method
*/

//named function
//function sum(a,b){return a + b}

//function expression
var sum = function(a, b){return a + b};
var product = function(a, b){return a * b};
console.log(sum)

// 2 case - function passed as argument 

var fun = function(a,b, func){
    return func(a,b);
}

var result = fun( 2, 3, product);

// returning a function

var greet = function(name){
    return function(message){
        return name + " "+ message; 
    }
}

var greetMessage = greet("Sunil");

var message = greetMessage("Welcome to Angular workshop !! ");
var goodMorning = greetMessage("Good Morning !!");

console.log(message);

var biFun = function(a, b, fun){
    return fun(a,b);
}

var add = biFun(4, 5, function(a,b){return a + b});
console.log(result)

var product =function(a){
    return function(){
        return a * a;
    }
}

var squareFun4 = product(4);
var squareFun7 = product(7);

console.log (squareFun4());


