var user = (function(){
    var first_name = '';
    var last_name = "";
    var age;
    return {
        fullName:function(){
            return first_name + " "+last_name;
        },
        setFirstName: function(fName){
            first_name = fName;
        },
        getFirstName: function(){
            return first_name;
        }
    }
}());

console.log(user.getFirstName());


user.setFirstName('Rakesh');
console.log(user.getFirstName());